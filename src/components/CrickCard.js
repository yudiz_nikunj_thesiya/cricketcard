import React from "react";
import "../styles/crickcard.scss";
import PropTypes from "prop-types";
import moment from "moment";
import placholderImg from "../assets/logo-placeholder.svg";
import WinnerCup from "../assets/winner-cup.ab728cae47f4b716f5e2.svg";

const CrickCard = ({ data }) => {
	const { sport_event, sport_event_status, statistics } = data;
	const matchDate = moment(sport_event.scheduled).format("Do MMMM YYYY");

	return (
		<div className="card">
			<div className="card-top">
				<div className="card-top--left">
					<span className="name">{sport_event.season.name}</span>
					<span className="teams">
						{sport_event.competitors[0].name} VS{" "}
						{sport_event.competitors[1].name}
					</span>
				</div>
				<div className="card-top--right">
					<span className="stadium">
						{sport_event.venue.name}, {sport_event.venue.city_name} |
					</span>
					<span className="match-date"> {matchDate}</span>
					<span className="result">{sport_event_status.match_result}</span>
				</div>
			</div>

			<div className="card-main">
				<div className="match">
					<div className="match-1">
						<div>
							<img src={placholderImg} className="match-placeholder" />
							<span className="team-abbreviation">
								{sport_event.competitors[1].abbreviation}
							</span>
						</div>
						<span className="team-run">
							{sport_event_status.period_scores[0].display_score}{" "}
							{sport_event_status.period_scores[0].display_overs} OV
						</span>
					</div>
					<div className="match-2">
						<div>
							<img src={placholderImg} className="match-placeholder" />
							<span className="team-abbreviation">
								{sport_event.competitors[0].abbreviation}
							</span>
							<img src={WinnerCup} className="winner-cup" alt="winner-cup" />
						</div>
						<span className="team-run">
							{sport_event_status.period_scores[1].display_score}{" "}
							{sport_event_status.period_scores[1].display_overs} OV
						</span>
					</div>
				</div>

				<div className="man-of-the-match">
					<img
						src={statistics.man_of_the_match[0].img}
						alt={statistics.man_of_the_match[0].name}
					/>
					<div className="heading">
						<span className="title">PLAYER OF THE MATCH</span>
						<span className="player-name">
							{statistics.man_of_the_match[0].name}
						</span>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CrickCard;

CrickCard.propTypes = {
	data: PropTypes.object,
};
