import React from "react";
import CrickCard from "./components/CrickCard";
import "./styles/app.scss";
import Data from "./data/cricket.json";

function App() {
	return (
		<div className="App">
			<div className="card-container">
				<CrickCard data={Data.data} />
				<CrickCard data={Data.data} />
			</div>
		</div>
	);
}

export default App;
